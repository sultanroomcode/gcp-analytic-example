<?php

// Load the Google API PHP Client Library.
require_once __DIR__ . '/vendor/autoload.php';

// Start a session to persist credentials.
// session_start();

// Create the client object and set the authorization configuration
// from the client_secrets.json you downloaded from the Developers Console.
$client = new Google_Client();
$client->setAuthConfig(__DIR__ . '/client_secrets.json');
$client->setRedirectUri('http://' . $_SERVER['HTTP_HOST'] . '/oauth2callback.php');
$client->addScope(Google_Service_Analytics::ANALYTICS_READONLY);
$client->setAccessType('offline');

// Handle authorization flow from the server.
if (! isset($_GET['code'])) {
  $auth_url = $client->createAuthUrl();
  header('Location: ' . filter_var($auth_url, FILTER_SANITIZE_URL));
} else {
  // $client->authenticate($_GET['code']);
  // $_SESSION['access_token'] = $client->getAccessToken();
  $redirect_uri = 'http://' . $_SERVER['HTTP_HOST'] . '/';
  $accessToken = $client->fetchAccessTokenWithAuthCode($_GET['code']);
	$client->setAccessToken($accessToken);

	// Check to see if there was an error.
	if (array_key_exists('error', $accessToken)) {
	    throw new Exception(join(', ', $accessToken));
	}
  // Save the token to a file.
  $tokenPath = 'client_token.json';
	if (!file_exists(dirname($tokenPath))) {
	    mkdir(dirname($tokenPath), 0700, true);
	}
	file_put_contents($tokenPath, json_encode($client->getAccessToken()));
  header('Location: ' . filter_var($redirect_uri, FILTER_SANITIZE_URL));
}

