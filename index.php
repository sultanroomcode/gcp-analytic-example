<?php
// Load the Google API PHP Client Library.
require_once __DIR__ . '/vendor/autoload.php';
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

// Start a session to persist credentials.
// session_start();

// Create the client object and set the authorization configuration
// from the client_secretes.json you downloaded from the developer console.
$client = new Google_Client();
$client->setAuthConfig(__DIR__ . '/client_secrets.json');
$client->addScope(Google_Service_Analytics::ANALYTICS_READONLY);
// $client->setAccessType('offline');

// If the user has already authorized this app then get an access token
// else redirect to ask the user to authorize access to Google Analytics.
$tokenPath = 'client_token.json';
if (file_exists($tokenPath)) {
    $accessToken = json_decode(file_get_contents($tokenPath), true);
    // var_dump($accessToken);
    $client->setAccessToken($accessToken);

    if ($client->isAccessTokenExpired()) {
    // Refresh the token if possible, else fetch a new one.
      if ($client->getRefreshToken()) {
          $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
      } else {
          // echo "Sudah habis";
          $redirect_uri = 'http://' . $_SERVER['HTTP_HOST'] . '/oauth2callback.php';
          header('Location: ' . filter_var($redirect_uri, FILTER_SANITIZE_URL));
      }
      // echo "Sudah habis lohh";
    }


// if (isset($_SESSION['access_token']) && $_SESSION['access_token']) {
  // Set the access token on the client.
  // $client->setAccessToken($_SESSION['access_token']);

  // Create an authorized analytics service object.
  $analytics = new Google_Service_Analytics($client);

  // Get the first view (profile) id for the authorized user.
  $profile = getFirstProfileId($analytics);

  // Get the results from the Core Reporting API and print the results.
  $results = getResults($analytics, $profile);
  $resultmonth = getMonthResults($analytics, $profile);
  $resulttoday = getTodayResults($analytics, $profile);
  $result2 = getResultsRealtime($analytics, $profile);
  
  $arr['result']['total_visitor_now'] = printResultsRealtime($result2);
  $arr['result']['total_visitor_today'] = printResults($resulttoday);
  $arr['result']['total_visitor_week'] = printResults($results);
  $arr['result']['total_visitor_month'] = printResults($resultmonth);

  echo json_encode($arr);
} else {
  $redirect_uri = 'http://' . $_SERVER['HTTP_HOST'] . '/oauth2callback.php';
  header('Location: ' . filter_var($redirect_uri, FILTER_SANITIZE_URL));
}




function getFirstProfileId($analytics) {
  // Get the user's first view (profile) ID.

  // Get the list of accounts for the authorized user.
  $accounts = $analytics->management_accounts->listManagementAccounts();

  //var_dump($accounts->getItems());

  if (count($accounts->getItems()) > 0) {
    $items = $accounts->getItems();
    $firstAccountId = $items[0]->getId();

    // Get the list of properties for the authorized user.
    $properties = $analytics->management_webproperties
        ->listManagementWebproperties($firstAccountId);

    if (count($properties->getItems()) > 0) {
      $items = $properties->getItems();
      $firstPropertyId = $items[0]->getId();

      // Get the list of views (profiles) for the authorized user.
      $profiles = $analytics->management_profiles
          ->listManagementProfiles($firstAccountId, $firstPropertyId);

          // var_dump($profiles->getItems());

      if (count($profiles->getItems()) > 0) {
        $items = $profiles->getItems();

        // Return the first view (profile) ID.
        return $items[0]->getId();

      } else {
        throw new Exception('No views (profiles) found for this user.');
      }
    } else {
      throw new Exception('No properties found for this user.');
    }
  } else {
    throw new Exception('No accounts found for this user.');
  }
}

function getMonthResults($analytics, $profileId) {
  // Calls the Core Reporting API and queries for the number of sessions
  // for the last seven days.
  return $analytics->data_ga->get(
      'ga:' . $profileId,
      '28daysAgo',
      'today',
      'ga:sessions');
}

function getTodayResults($analytics, $profileId) {
  // Calls the Core Reporting API and queries for the number of sessions
  // for the last seven days.
  return $analytics->data_ga->get(
      'ga:' . $profileId,
      'today',
      'today',
      'ga:sessions');
}

function getResults($analytics, $profileId) {
  // Calls the Core Reporting API and queries for the number of sessions
  // for the last seven days.
  return $analytics->data_ga->get(
      'ga:' . $profileId,
      '7daysAgo',
      'today',
      'ga:sessions');
}

function getResultsRealtime($analytics, $profileId) {
  // Calls the Core Reporting API and queries for the number of sessions
  // for the last seven days.
  return $analytics->data_realtime->get(
      'ga:' . $profileId,
      'rt:activeUsers');
}

function printResults($results) {
  // var_dump($results);
  // Parses the response from the Core Reporting API and prints
  // the profile name and total sessions.
  if ($results->getRows() != null && count($results->getRows()) > 0) {

    // Get the profile name.
    $profileName = $results->getProfileInfo()->getProfileName();

    // Get the entry for the first entry in the first row.
    $rows = $results->getRows();

    $sessions = $rows[0][0];

    // Print the results.
    // print "<p>First view (profile) found: $profileName</p>";
    return $sessions;
  } else {
    return 0;
  }
}

function printResultsRealtime($results) {
  // Parses the response from the Core Reporting API and prints
  // the profile name and total sessions.
  
  if ($results->getRows() != null && count($results->getRows()) > 0) {

    // Get the profile name.
    $profileName = $results->getProfileInfo()->getProfileName();

    // Get the entry for the first entry in the first row.
    $rows = $results->getRows();

    $sessions = $rows[0][0];

    // Print the results.
    // print "<p>First view (profile) found: $profileName</p>";
    return $sessions;
  } else {
    return 0;
  }
}

?>
